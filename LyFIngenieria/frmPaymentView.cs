﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LyFIngenieria.Clases;
using System.Data.SQLite;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace LyFIngenieria
{
    public partial class frmPaymentView : Form
    {
        DeviceLibrary.DeviceLibrary deviceLibrary = new DeviceLibrary.DeviceLibrary();

        decimal restante = 0;
        decimal depositado = 0;
        int _countBill = 0;
        int _countCoins = 0;
        int _count = 0;

        public frmPaymentView()
        {
            InitializeComponent();
            deviceLibrary = new DeviceLibrary.DeviceLibrary();

            deviceLibrary.AcceptedDocument += DeviceLibrary_AcceptedDocument;
        }

        private void DeviceLibrary_AcceptedDocument(DeviceLibrary.Models.Document obj)
        {
            var _obj = obj;
            if (obj.Type == DeviceLibrary.Models.Enums.DocumentType.Bill)
            {
                _countBill += 1;
            }
            else
            {
                _countCoins += 1;
            }

            depositado += obj.Value;

            btnCancelar.Enabled = false;
            insertTransaction(clsDatos.noCuenta, obj.Value);

        }

        private void frmPaymentView_Load(object sender, EventArgs e)
        {
            //double _deuda = Convert.ToDouble(clsDatos.debt);
            //double _depositado
            //double _restante = Convert.ToDouble(clsDatos.debt);

            lblDeuda.Text = "$ " + clsDatos.debt;
            lblDepositado.Text = "$ 0.00";
            lblRestante.Text = "$ " + clsDatos.debt;


            var deviceStatus = deviceLibrary.Status;

            if (deviceStatus == DeviceLibrary.Models.Enums.DeviceStatus.Disconnected)
            {
                deviceLibrary.Open();
            }

            

        }

        private bool insertarBD(string usuario, string cuenta, double debt, double paid)
        {
            try
            {
                var sql = $"insert into tblDatos (Customer, Account, Debt, Paid) values ('{usuario}', '{cuenta}', {debt}, {paid})";
                SQLiteCommand cmd = new SQLiteCommand(sql, clsConexion.instanciaDB());

                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error InsertarBD(): " + ex.ToString());
                return false;
            }
           

        }

        private void insertTransaction(string account, decimal paid)
        {
            var url = $"https://api.xenterglobal.com:2053/transaction?token=960be5faef11e6741bc1881de8b7d0ce";
            var request = (HttpWebRequest)WebRequest.Create(url);
            string json = $"{{\"account\":\"{account}\",\"paid\":\"{paid}\"}}";
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {

                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null)
                        {
                            return;
                        }
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();

                            dynamic _datos = JsonConvert.DeserializeObject(responseBody);

                            restante = Convert.ToDecimal(_datos.debt);

                            lblDepositado.Text = "$ " + depositado.ToString();
                            lblRestante.Text = "$ " + restante.ToString();
                        }
                    }
                }

                if (restante <= 0)
                {
                    insertarAbono(true);
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error insertaTransaccion: " + ex.Message);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            string btnName = (sender as Button).Name;

            decimal _valor = 0;

            switch (btnName)
            {
                case "btn5c":
                    _valor = Convert.ToDecimal(0.05);
                    break;
                default:
                    _valor = Convert.ToDecimal(btnName.Replace("btn", "").Trim());
                    break;
            }

            var _tipo = DeviceLibrary.Models.Enums.DocumentType.Bill;

            if (_valor < 20)
            {
                _tipo = DeviceLibrary.Models.Enums.DocumentType.Coin;

                if (_countCoins == 0)
                {
                    _countCoins += 1;
                    _count = _countCoins;
                }
            }
            else
            {
                _countBill += 1;
                _count = _countBill;
            }
            
            var document = new DeviceLibrary.Models.Document(_valor, _tipo, _count);

            deviceLibrary.SimulateInsertion(document);

            //MessageBox.Show(_valor.ToString());

        }

        private void btnAbonar_Click(object sender, EventArgs e)
        {
            insertarAbono(false);

        }

        private void insertarAbono(bool completo)
        {

            var insertado = insertarBD(clsDatos.user, clsDatos.noCuenta, Convert.ToDouble(clsDatos.debt), Convert.ToDouble(depositado));

            if (insertado)
            {
                if (completo)
                {
                    MessageBox.Show("Pago Completo...");
                }
                else
                {
                    MessageBox.Show("Abono Completo...");
                }
            }
            
            

            this.Close();
        }
    }
}
