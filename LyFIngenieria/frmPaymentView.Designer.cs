﻿
namespace LyFIngenieria
{
    partial class frmPaymentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDeuda = new System.Windows.Forms.Label();
            this.lblDepositado = new System.Windows.Forms.Label();
            this.lblRestante = new System.Windows.Forms.Label();
            this.btn500 = new System.Windows.Forms.Button();
            this.btn200 = new System.Windows.Forms.Button();
            this.btn100 = new System.Windows.Forms.Button();
            this.btn50 = new System.Windows.Forms.Button();
            this.btn20 = new System.Windows.Forms.Button();
            this.btn10 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn5c = new System.Windows.Forms.Button();
            this.btnAbonar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(770, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "Payment View";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(46, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(202, 40);
            this.label4.TabIndex = 21;
            this.label4.Text = "Deuda";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(284, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 40);
            this.label2.TabIndex = 22;
            this.label2.Text = "Depositado";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(532, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 40);
            this.label3.TabIndex = 23;
            this.label3.Text = "Restante";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDeuda
            // 
            this.lblDeuda.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeuda.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblDeuda.Location = new System.Drawing.Point(46, 149);
            this.lblDeuda.Name = "lblDeuda";
            this.lblDeuda.Size = new System.Drawing.Size(202, 40);
            this.lblDeuda.TabIndex = 24;
            this.lblDeuda.Text = "$ 123.45";
            this.lblDeuda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDepositado
            // 
            this.lblDepositado.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepositado.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblDepositado.Location = new System.Drawing.Point(284, 149);
            this.lblDepositado.Name = "lblDepositado";
            this.lblDepositado.Size = new System.Drawing.Size(202, 40);
            this.lblDepositado.TabIndex = 25;
            this.lblDepositado.Text = "$ 50.00";
            this.lblDepositado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRestante
            // 
            this.lblRestante.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRestante.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblRestante.Location = new System.Drawing.Point(532, 149);
            this.lblRestante.Name = "lblRestante";
            this.lblRestante.Size = new System.Drawing.Size(202, 40);
            this.lblRestante.TabIndex = 26;
            this.lblRestante.Text = "$ 73.45";
            this.lblRestante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn500
            // 
            this.btn500.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn500.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn500.Location = new System.Drawing.Point(125, 244);
            this.btn500.Name = "btn500";
            this.btn500.Size = new System.Drawing.Size(89, 37);
            this.btn500.TabIndex = 27;
            this.btn500.Text = "$500.00";
            this.btn500.UseVisualStyleBackColor = true;
            this.btn500.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn200
            // 
            this.btn200.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn200.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn200.Location = new System.Drawing.Point(241, 244);
            this.btn200.Name = "btn200";
            this.btn200.Size = new System.Drawing.Size(89, 37);
            this.btn200.TabIndex = 28;
            this.btn200.Text = "$200.00";
            this.btn200.UseVisualStyleBackColor = true;
            this.btn200.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn100
            // 
            this.btn100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn100.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn100.Location = new System.Drawing.Point(357, 244);
            this.btn100.Name = "btn100";
            this.btn100.Size = new System.Drawing.Size(89, 37);
            this.btn100.TabIndex = 29;
            this.btn100.Text = "$100.00";
            this.btn100.UseVisualStyleBackColor = true;
            this.btn100.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn50
            // 
            this.btn50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn50.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn50.Location = new System.Drawing.Point(469, 244);
            this.btn50.Name = "btn50";
            this.btn50.Size = new System.Drawing.Size(89, 37);
            this.btn50.TabIndex = 30;
            this.btn50.Text = "$50.00";
            this.btn50.UseVisualStyleBackColor = true;
            this.btn50.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn20
            // 
            this.btn20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn20.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn20.Location = new System.Drawing.Point(581, 244);
            this.btn20.Name = "btn20";
            this.btn20.Size = new System.Drawing.Size(89, 37);
            this.btn20.TabIndex = 31;
            this.btn20.Text = "$20.00";
            this.btn20.UseVisualStyleBackColor = true;
            this.btn20.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn10
            // 
            this.btn10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn10.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn10.Location = new System.Drawing.Point(186, 309);
            this.btn10.Name = "btn10";
            this.btn10.Size = new System.Drawing.Size(62, 37);
            this.btn10.TabIndex = 32;
            this.btn10.Text = "$10";
            this.btn10.UseVisualStyleBackColor = true;
            this.btn10.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5
            // 
            this.btn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn5.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(270, 309);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(60, 37);
            this.btn5.TabIndex = 33;
            this.btn5.Text = "$5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2
            // 
            this.btn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(357, 309);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(62, 37);
            this.btn2.TabIndex = 34;
            this.btn2.Text = "$2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn1
            // 
            this.btn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(447, 309);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(60, 37);
            this.btn1.TabIndex = 35;
            this.btn1.Text = "$1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5c
            // 
            this.btn5c.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn5c.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5c.Location = new System.Drawing.Point(539, 309);
            this.btn5c.Name = "btn5c";
            this.btn5c.Size = new System.Drawing.Size(58, 37);
            this.btn5c.TabIndex = 36;
            this.btn5c.Text = "c5";
            this.btn5c.UseVisualStyleBackColor = true;
            this.btn5c.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAbonar
            // 
            this.btnAbonar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbonar.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbonar.Location = new System.Drawing.Point(582, 396);
            this.btnAbonar.Name = "btnAbonar";
            this.btnAbonar.Size = new System.Drawing.Size(176, 67);
            this.btnAbonar.TabIndex = 37;
            this.btnAbonar.Text = "Abonar >";
            this.btnAbonar.UseVisualStyleBackColor = true;
            this.btnAbonar.Click += new System.EventHandler(this.btnAbonar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(12, 396);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(176, 67);
            this.btnCancelar.TabIndex = 38;
            this.btnCancelar.Text = "< Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // frmPaymentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(770, 502);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAbonar);
            this.Controls.Add(this.btn5c);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn10);
            this.Controls.Add(this.btn20);
            this.Controls.Add(this.btn50);
            this.Controls.Add(this.btn100);
            this.Controls.Add(this.btn200);
            this.Controls.Add(this.btn500);
            this.Controls.Add(this.lblRestante);
            this.Controls.Add(this.lblDepositado);
            this.Controls.Add(this.lblDeuda);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmPaymentView";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPaymentView";
            this.Load += new System.EventHandler(this.frmPaymentView_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDeuda;
        private System.Windows.Forms.Label lblDepositado;
        private System.Windows.Forms.Label lblRestante;
        private System.Windows.Forms.Button btn500;
        private System.Windows.Forms.Button btn200;
        private System.Windows.Forms.Button btn100;
        private System.Windows.Forms.Button btn50;
        private System.Windows.Forms.Button btn20;
        private System.Windows.Forms.Button btn10;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn5c;
        private System.Windows.Forms.Button btnAbonar;
        private System.Windows.Forms.Button btnCancelar;
    }
}