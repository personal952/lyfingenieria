﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LyFIngenieria.Clases;

namespace LyFIngenieria
{
    public partial class frmBalanceView : Form
    {
        public frmBalanceView()
        {
            InitializeComponent();
        }

        private void frmBalanceView_Load(object sender, EventArgs e)
        {
            lblCuenta.Text = clsDatos.noCuenta;
            lblUsuario.Text = clsDatos.user;
            lblDeuda.Text = "$" + clsDatos.debt;

        }

        private void btnPagar_Click(object sender, EventArgs e)
        {
            frmPaymentView frmPayment = new frmPaymentView();
            frmPayment.ShowDialog();

            this.Close();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            frmAcountView frmAcount = new frmAcountView();
            frmAcount.Close();

            this.Close();
        }
    }
}
