﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LyFIngenieria.Clases;
using RestSharp;

namespace LyFIngenieria
{
    public partial class frmAcountView : Form
    {
        public string _noCuenta;
        public string _usuario;
        public string _debt;

        bool regresaDatos = false;
        public frmAcountView()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            if (txtNumeroCuenta.Text == "Numero de cuenta...")
            {
                txtNumeroCuenta.Text = "";
            }

            string btnText = (sender as Button).Text;

            txtNumeroCuenta.Text += btnText; 
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {

            GetAccount(txtNumeroCuenta.Text);

            if (regresaDatos)
            {
                frmBalanceView frmBalance = new frmBalanceView();
                frmBalance.ShowDialog();

                this.Close();
            }
            else
            {
                MessageBox.Show("Por favor verifique la cuenta...");
                txtNumeroCuenta.Text = "Numero de cuenta...";
            }
        }

        private void GetAccount(string _account)
        {
            var url = $"https://api.xenterglobal.com:2053/account_balance?token=960be5faef11e6741bc1881de8b7d0ce&account={_account}";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null)
                        {
                            return;
                        }
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();

                            dynamic _datos = JsonConvert.DeserializeObject(responseBody);

                            clsDatos.noCuenta = txtNumeroCuenta.Text;
                            clsDatos.user = _datos.user;
                            clsDatos.debt = _datos.debt;

                            regresaDatos = true;
                        }
                    }
                }
            }
            catch (WebException ex)
            {

            }
        }

        private void frmAcountView_Load(object sender, EventArgs e)
        {
            regresaDatos = false;
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            int x = 0;
            string borrado = txtNumeroCuenta.Text;
            x = borrado.Length - 1;

            borrado = borrado.Substring(0, x);

            txtNumeroCuenta.Text = borrado;

            if (txtNumeroCuenta.Text == "")
            {
                txtNumeroCuenta.Text = "Numero de cuenta...";
            }
        }
    }
}
