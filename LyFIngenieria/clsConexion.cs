﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace LyFIngenieria
{
    class clsConexion
    {
        private static Conexion_Instancia instancia = null;

        public static SQLiteConnection instanciaDB()
        {
            if (instancia == null)
            {
                instancia = new Conexion_Instancia();
            }

            return instancia._conn;
        }
    }

    public class Conexion_Instancia
    {
        public SQLiteConnection _conn = null;

        public Conexion_Instancia()
        {
            _conn = new SQLiteConnection(@"Data Source=c:\Users\luiscarlosgalvez\source\repos\LyFIngenieria\db\DB_LyF.s3db;Version=3;");
            _conn.Open();
        }

        ~Conexion_Instancia()
        {
            _conn.Close();
        }

    }
}
